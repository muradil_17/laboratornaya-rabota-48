import React, {Component, Fragment} from 'react';
import Post from "./Post";

class GetRespons extends Component {
    constructor(props) {
        super(props);

        this.state = {
            todos: []
        };
    }

    componentDidMount() {
        fetch('http://146.185.154.90:8000/messages').then(response =>{
            if (response.ok){
                return response.json();
            }
            throw new Error("Error mother fucker");
        }).then(posts =>{
            const UserPost = posts.map(userName =>{
                return{
                    ...userName
                }
            });
            this.setState({todos: UserPost})
        }).catch(error =>{
            console.log(error);
        })
    }

    render() {
        return (
            <Fragment>
                <section>
                    {this.state.todos.map(post =>(
                        <Post
                            key = {post.id}
                            message = {post.message}
                            author = {post.author}
                            datetime = {post.datetime}
                        />
                    ))}
                </section>
            </Fragment>
        )
    }

}

export default GetRespons;