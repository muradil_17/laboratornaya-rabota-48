import React, {Component} from 'react';

class GetDialog extends Component {
    constructor(props) {
        super(props);
        this.state = { value: ''};

        this.UserMessage = this.UserMessage.bind(this);
        this.UserShowmessage = this.UserShowmessage.bind(this);
    }

    UserMessage(event){
        this.setState({value: event.target.value});
    }

    UserShowmessage(event){
        event.preventDefault();
        console.log(this.state.value)
    }



    render() {
        return (
            <div>
                <form>
                    <input type="text" name="user" placeholder="Написать сообщение... " onChange={this.UserMessage} value={this.state.value}/>
                    <input type="button" value="отправить" onClick={this.UserShowmessage} />
                </form>
            </div>
        );
    }
}

export default GetDialog;