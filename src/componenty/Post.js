import React, {Component} from 'react';
import '../style/Post.css'
class Post extends Component {

    render() {
        return (
            <article>
                <div className="container">
                    <h1>Author: {this.props.author}</h1>
                    <p>Message: {this.props.message}</p>
                    <h3>Дата: {this.props.datetime}</h3>
                </div>
            </article>
        );
    }
}

export default Post;